﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class handles the infotainment centerstack updates.
/// For now color changes are supported.
/// TODO: Add nice graphics.
/// </summary>
public class ScreenConfig : MonoBehaviour
{
    /// <summary>
    /// For acessing the car configuration parameters. </summary>
    public CarConfiguration carConfiguration;
    /// <summary>
	/// For indicating that ignition is on. </summary>
	private bool ignitionOn;
    /// <summary>
    /// Colors to be used. </summary>
    private Color onColor, offColor;


    // Use this for initialization
    void Start()
    {
        onColor = new Color(0.35f, 0.35f, 0.35f, 35f);
        offColor = new Color(0f, 0f, 0f, 0f);
    }

    void Update()
    {
    }

    /// <summary>
    /// Sets the center stack infotainment color. 
    /// This will hopefully be replaced at some time with proper graphics
    /// or at least an image.
    /// <summary>
    void setCenterstackColor()
    {
        ignitionOn = !ignitionOn;
        Color c = ignitionOn ? onColor : offColor;

        Material material = GetComponent<Renderer>().material;

        Renderer rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Standard");
        rend.material.color = c;
    }

}
