﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarConfiguration : MonoBehaviour
{
    /// <summary>
    /// Manipulates the Oculus camera.
    /// </summary>
    public RiftManipulator rift;

    /// <summary>
    /// The Oculus camera position in the EgoCar.
    /// </summary>
    public enum CameraPositon
    {
        DriverSeat, FrontPassenger, RearLeftPassenger, RearRightPassenger,
    }

    /// <summary>
    /// The current camera position of the Oculus camera.
    /// The default position shall be in the driver seat.
    /// </summary>
    public CameraPositon currentCameraPosition;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        updateCameraPosition();
    }

	/// <summary>
	/// Updates the position of the Oculus camera in VR. 
	/// </summary>
    private void updateCameraPosition()
    {
        switch (currentCameraPosition)
        {
            case CameraPositon.DriverSeat:
                rift.IsDriverSeat = true;
                rift.moveCamera(0, 0, 0);
                break;

            case CameraPositon.FrontPassenger:
                rift.IsDriverSeat = false;
                rift.moveCamera(0.71f, 0.0f, 0.0f);
                break;

            case CameraPositon.RearLeftPassenger:
                rift.IsDriverSeat = false;
                rift.moveCamera(-0.08f, -0.16f, -0.83f);
                break;

            case CameraPositon.RearRightPassenger:
                rift.IsDriverSeat = false;
                rift.moveCamera(0.71f, -0.16f, -0.83f);
                break;

            default:
                rift.moveCamera(0, 0, 0);
                break;
        }

    }
}
