﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;
using System.IO;
using System;

public class RiftLogger : MonoBehaviour
{
    private string path;
    private StreamWriter streamWriter;

    // Use this for initialization
    void Start()
    {
		// Create the silly path
		DateTime localDate = DateTime.Now;
		string time = localDate.ToString().Replace(' ', '_').Replace(':', '-');
        path = "Assets/LogData/log_file_" + time + ".txt";

        streamWriter = new StreamWriter(path, true);
		streamWriter.WriteLine("pos_x, pos_y, pos_z, rot_x, rot_y, rot_z");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Log position
        float x = transform.localPosition.x;
        float y = transform.localPosition.y;
        float z = transform.localPosition.z;

        // Log rotation, convert to degrees.
        float xr = transform.localRotation.x * 180;
        float yr = transform.localRotation.y * 180;
        float zr = transform.localRotation.z * 180;

        //Debug.Log("pos x " + x.ToString() + " y " + y.ToString() + " z " + z.ToString());
        //Debug.Log("rot x " + xr.ToString() + " y " + yr.ToString() + " z " + zr.ToString());

        streamWriter.WriteLine(x.ToString() + ", " + y.ToString() + ", " + z.ToString() + ", " +
                                xr.ToString() + ", " + yr.ToString() + ", " + zr.ToString());
		streamWriter.Flush();


    }

}
