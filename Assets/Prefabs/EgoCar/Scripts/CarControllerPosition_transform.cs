﻿using System;
using UnityEngine;

public class CarControllerPosition_transform : MonoBehaviour
{
    private Rigidbody rb;
	public SteeringWheelTurning steeringWheelTurning;
    public RiftManipulator riftManipulator;
	private bool firstUpdate;
	public Transform otherObjects;
    private float prevHeading;

    // Use this for initialization
    void Start()
    {
		firstUpdate = true;
        prevHeading = 0;
    }

    public void update_coordinates(float x, float y, float z, float heading)
    {
        try
        {		
			if (firstUpdate)
			{				
				prevHeading = heading;
                firstUpdate = false;                

				// Rotate the virtual world with the initial heading. This will make the car go straight from the start position
                if(otherObjects != null)
				    otherObjects.rotation = Quaternion.Euler(0, -heading - 14.8f, 0);
			}
            
			transform.rotation = Quaternion.Euler(0, -heading -14.8f, 0);
			transform.position = new Vector3(-x, y, z);

            float compensation = heading - prevHeading;
            riftManipulator.antiRotateY(compensation);
            
			prevHeading = heading;
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }

}
