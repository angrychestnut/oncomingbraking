﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheelTurning : MonoBehaviour
{
    public ReceiveData recData;

    private void FixedUpdate()
    {
        float angle = recData.SteeringWheelAngle;
        //Debug.Log("Angle int SteeringWheelTurning = " + angle.ToString());
        //transform.Rotate(new Vector3(angle, 0, 0), Space.Self);
        transform.localRotation = Quaternion.Euler(angle, transform.rotation.y, transform.rotation.z);
    }
}
