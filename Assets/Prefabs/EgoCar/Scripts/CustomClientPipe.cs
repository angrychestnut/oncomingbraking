﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;

namespace Assets.Prefabs.EgoCar.Scripts
{
    public class CustomClientPipe
    {
        private NamedPipeClientStream _client;
        private StreamReader _reader;
        private StreamWriter _writer;
        private string _pipe;

        public CustomClientPipe(String pipename)
        {
            _pipe = pipename;
            _client = new NamedPipeClientStream(".", _pipe, PipeDirection.InOut);
            _reader = new StreamReader(_client);
            _writer = new StreamWriter(_client);  
        }

        public void Connect()
        {
            try
            {
                _client.Connect();
                Console.WriteLine("[thread: {0}] -> Connected to Real time platform.", Thread.CurrentThread.ManagedThreadId);
            } catch (Exception e)
            {
				Console.WriteLine("Nothing to connect to. Exception: " + e);
            }
        }

        public async Task<string> ReadDataAsync()
        {
            string result = await _reader.ReadLineAsync();
            Console.WriteLine("[thread: {0}] -> Received: {1} from Real Time Platform", Thread.CurrentThread.ManagedThreadId, result);

            return result;
        }

        public void WriteData(string data)
        {
            _writer.WriteLine(data);
            Console.WriteLine("[thread: {0}] -> Sent: {1} on {2}", Thread.CurrentThread.ManagedThreadId, data, _pipe);
        }
    }
}
