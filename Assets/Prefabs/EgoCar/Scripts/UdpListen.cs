﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Udp
{
    public struct Received
    {
        public IPEndPoint Sender;
        public string Message;
    }

    public class UdpListen
    {
        private UdpClient Client;
        private IPEndPoint _listenOn;

        // Default contructor listening to all traffic in port 3000
        public UdpListen() : this(new IPEndPoint(IPAddress.Any, 3000))
        {
        }

        // Constructor, but define the endpoint yourself
        public UdpListen(IPEndPoint endpoint)
        {
            _listenOn = endpoint;
            Client = new UdpClient(_listenOn)
            {
                EnableBroadcast = true,
                MulticastLoopback = true
            };
        }

        public UdpListen(IPAddress multicast, IPAddress local)
        {
            Client = new UdpClient(AddressFamily.InterNetwork);
            Client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            Client.Client.Bind(new IPEndPoint(local, 3000));
            Client.JoinMulticastGroup(multicast, local);
        }

        public async Task<Received> Receive()
        {
            var result = await Client.ReceiveAsync();

            return new Received()
            {
                Message = BitConverter.ToString(result.Buffer),
                Sender = result.RemoteEndPoint
            };
        }
    }
}

