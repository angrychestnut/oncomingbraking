﻿using System;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using Assets.Prefabs.EgoCar.Scripts;

public class PositionUpdater : MonoBehaviour {
    private bool first_packet;

	private double x0, y0;//, z0;
    private float roll, pitch, heading;
    private RTKData data;
    private static CustomClientPipe positionPipe;


    public struct RTKData
    {
        public double x, y, z;
        public float heading, pitch, roll;
    }

    // Use this for initialization
    void Start () 
	{
        first_packet = true;

        positionPipe = new CustomClientPipe("PositionPipe");

        Thread packet_handler = new Thread(new ThreadStart(ListenPositionData));
        packet_handler.Start();

        InvokeRepeating("RenderPosition", 0.0f, 0.01f);
    }


    void RenderPosition()
    {
		float d_heading = data.heading;

        d_heading = d_heading * (float)(180 / Math.PI);
		d_heading *= -1;

        float dx = Convert.ToSingle(data.x - x0);
        float dy = Convert.ToSingle(data.y - y0);
        //float dz = Convert.ToSingle(data.z - z0);

        try
        {
			//start to send coordinates to the car when the first packet is received
			if (!first_packet)
			{
            	gameObject.GetComponent<CarControllerPosition_transform>().update_coordinates(dy, 0, dx, d_heading);
			}

        }
        catch (IndexOutOfRangeException e)
        {
            Debug.Log(e.Message);
        }
    }

    void ListenPositionData()
    {
        Task.Run(async () => {
            positionPipe.Connect();

            while (true) // Some kind of canceller.
            {
                try
                {
                    var json = await positionPipe.ReadDataAsync();
                    data = JsonUtility.FromJson<RTKData>(json);

                    if (first_packet)
                    {
                        x0 = data.x;
                        y0 = data.y;
                        //z0 = data.z;

                        first_packet = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        });
    }

}
