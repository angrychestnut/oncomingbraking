﻿using System;
using System.Linq;
using UnityEngine;

namespace DParser {
    class DataParser
    {
        // Formats part of a byte array (represented as hex) to correct format
        // Little endian to big endian, returns a string.
        private string FormatHex(String[] bytes, int index, int s)
        {
            // Copies s bytes with index as starting byte.
            String[] arr = bytes.Skip(index).Take(s).ToArray();
            Array.Reverse(arr); // Little to big endian
            string str = String.Join("", arr);
            return str;
        }

        // Formats 24-bit signed hex string into 32 bit double.
        // Hex -> Binary (-> pad correctly to 32 bit) -> Double
        private float SignedWordToFloat(string hex)
        {
            string binary = String.Join(String.Empty,
                hex.Select(
                c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')
                )
            );

            binary = binary.PadLeft(32, binary[0]); // Pads with MSB on the left 8 bits
            return Convert.ToSingle(Convert.ToInt32(binary, 2));
        }

        // Here we can add Debug checks for data validity 
        private void CheckDataValidity(String[] bytes)
        {
            
            if (bytes[62] == "1B")
            {
                float heading_q = Convert.ToInt32(bytes[63], 16);
                if (0 <= heading_q & heading_q <= 3)
                {
                    Debug.Log("Heading quality: " + heading_q);
                }
            }
        }

        public void ParseRtkData(String packet, ref double latitude, ref double longitude, ref float altitude, ref float heading, ref float pitch, ref float roll, ref float nav_status)
        {
            String[] bytes = packet.Split('-');

            // The information about what bytes to use can
            // be found in the RTK datasheet.
            string latitude_hex = FormatHex(bytes, 23, 8);
            string longitude_hex = FormatHex(bytes, 31, 8);
            string altitude_hex = FormatHex(bytes, 39, 4);

            string heading_hex = FormatHex(bytes, 52, 3);
            string pitch_hex = FormatHex(bytes, 55, 3);
            string roll_hex = FormatHex(bytes, 58, 3);

            CheckDataValidity(bytes);

            latitude = BitConverter.Int64BitsToDouble(Convert.ToInt64(latitude_hex, 16));
            longitude = BitConverter.Int64BitsToDouble(Convert.ToInt64(longitude_hex, 16));

            int altitude_int = Convert.ToInt32(altitude_hex, 16);
            byte[] altitude_arr = BitConverter.GetBytes(altitude_int);
            altitude = BitConverter.ToSingle(altitude_arr, 0);

            float tenPOWSix = Convert.ToSingle(Math.Pow(10, 6));

            heading = SignedWordToFloat(heading_hex) / tenPOWSix;
            pitch = SignedWordToFloat(pitch_hex) / tenPOWSix;
            roll = SignedWordToFloat(roll_hex) / tenPOWSix;
            nav_status = Convert.ToInt32(bytes[21], 16);
        }

        // Formats longitude, latitude, and altitude to Cartesian coordinates (x, y, z). 
        // x, y, z is returned as metres.
        public void CartesianFormat(double latitude, double longitude, double altitude, ref double x, ref double y, ref double z)
        {
            // Conversion absed on WGS84 World Geodetic System
            double a = 6378137; // semi-major axis value in metres
            double e2 = 0.00669437999014; // first eccentricity value - 6.69437999014 x 10-3

            double N = a / (Math.Sqrt(1 - (e2 * Math.Pow(Math.Sin(latitude), 2))));
            // x, y, z values from the center of mass of the earth in metres
            x = (N + altitude) * Math.Cos(latitude) * Math.Cos(longitude);
            y = (N + altitude) * Math.Cos(latitude) * Math.Sin(longitude);
            z = ((N * (1 - e2)) + altitude) * Math.Sin(latitude);
        }
    }
}
