﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// This object sets sample text in the infotainment. It gets data from CAN/FLEXRAY
/// by comunicating with ReceiveData.
/// </summary>
public class Infotainment : MonoBehaviour {

	public Text temperature1, temperature2, clock;
	public ReceiveData recData;
	
	/// <summary>
	/// Updates the clock and temperatures text strings.
	/// </summary>
	void FixedUpdate()
	{
		clock.text = DateTime.Now.ToString("HH:mm");

		int tempLeft = recData.TemperatureLeft;
		temperature1.text = tempLeft < 17 ? "LO" : tempLeft.ToString();
		//temperature1.text =  "21";

		int tempRight = recData.TemperatureRight;
		temperature2.text = tempRight < 17 ? "LO" : tempRight.ToString();
		//temperature2.text =  "21";
	}
}
