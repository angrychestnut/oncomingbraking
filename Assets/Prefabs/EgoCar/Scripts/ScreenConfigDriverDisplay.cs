﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summar>
/// This class sets the graphics for the driver display in
/// the ego car.
/// </summary>
public class ScreenConfigDriverDisplay : MonoBehaviour
{
	/// <summary>
	/// Materials used in the display. </summary>
    public Material vehInFrontMaterial, normalMaterial, lineTooCloseMaterial, turnIndMaterial;
    /// <summary>
	/// Section of the DIM displaying collision warning </summary>
    private GameObject collisionWarning;
    /// <summary>
    /// Renderer that is used to change collisionWarning material with. </summary>
	private Renderer carRend;
	/// <summary>
	/// Renderer that is used to change left turn indication material with. </summary>
	private Renderer leftRend;
	/// <summary>
	/// Renderer that is used to change right turn indication material with. </summary>
	private Renderer rightRend;
    /// <summary>
    /// For knowing if the materials are used. </summary>
    private bool carInFrontActive = false;
    /// <summary>
    /// Shows ego car's speed.
    /// </summary>
    public Text carSpeedText;
	/// <summary>
	/// Shows ego car's active gear.
	/// </summary>
	public Text gearText;
    /// <summary>
    /// To read data from Flexray/CAN that is needed here.
    /// </summary>
    public ReceiveData recData;
	/// <summary>
	/// Arrow in the dim showing turn indication to the left
	/// </summary>
	private GameObject turnLeft;
	/// <summary>
	/// Arrow in the dim showing turn indication to the right
	/// </summary>
	private GameObject turnRight;
	/// <summary>
	/// Bool showing if the rigth turn indicator is active
	/// </summary>
	private bool rightInd = false;
	/// <summary>
	/// Bool showing if the left turn indicator is active
	/// </summary>
	private bool leftInd = false;
	/// <summary>
	/// Time since the right turn indicator lamp was turned on/off
	/// </summary>
	private float indTime = 0;
	/// <summary>
	/// Indicated if the lamp is on
	/// </summary>
	private bool lampActive = false;
	/// <summary>
	/// The time the indicator lamps are turned off/on
	/// </summary>
	private float BLINK_TIME = 0.5f;

    void Start()
    {
        Material material = GetComponent<Renderer>().material;
        
		collisionWarning = GameObject.Find("CarWarningArea");
		carRend = collisionWarning.GetComponent<Renderer>();

		turnLeft = GameObject.Find ("TurningArrowLeft");
		turnRight = GameObject.Find ("TurningArrowRight");
		leftRend = turnLeft.GetComponent<Renderer>();
		rightRend = turnRight.GetComponent<Renderer>();
	
        carRend.material = normalMaterial;
    }

	/// <summary>
	/// Updates the driver display with fancy graphics. 
	/// If the ego car has a car in front it will show a car in the
	/// display, otherwise nothing.
	/// </summary>
	/// <param name="carInFront">Defines if there is a car in front of the ego car.</param>
    public void setDriverDisplayCarInFront(bool carInfront)
    {   
        if (carInfront == carInFrontActive)
            return;

        carInFrontActive = carInfront;
		carRend.material = carInfront ? vehInFrontMaterial : normalMaterial;
    }

	private void disableTurnInd()
	{
		if (leftInd) {
			leftRend.material = normalMaterial;
			leftInd = false;
		} else if (rightInd) 
		{
			rightRend.material = normalMaterial;
			rightInd = false;
		}
	}

	private void turnInd()
	{
		if (indTime > BLINK_TIME) 
		{
			if (lampActive) 
			{
				if (leftInd) 
				{
					leftRend.material = normalMaterial;
				} else if (rightInd)
				{
					rightRend.material = normalMaterial;
				}
				lampActive = false;
			} else 
			{
				if (leftInd) 
				{ 
					leftRend.material = turnIndMaterial;
				} else if (rightInd)
				{
					rightRend.material = turnIndMaterial;
				}
				lampActive = true;
			}

			indTime = 0;
		}

		indTime += Time.fixedDeltaTime;
	}

    private void FixedUpdate()
    {   
        if (recData != null)
        {
			//display car speed
            carSpeedText.text = recData.EgoCarSpeed.ToString();

			//display turn indicator status
			switch (recData.TurnIndicator) 
			{
				case 0:
					disableTurnInd ();
					break;
				case 1:
					if (!leftInd) 
					{ 
						//is needed if the signal doesn't go to 0 before changing direction. To turn off right
						disableTurnInd();

						leftInd = true;

						//set leftTime to a big value to trigger the if statement in the function
						indTime = 99.0f;
					}	
					turnInd ();
					break;
				case 2:
					if (!rightInd) 
					{
						//is needed if the signal doesn't go to 0 before changing direction
						disableTurnInd();

						rightInd = true;

						//set rightTime to a big value to trigger the if statement in the function
						indTime = 99.0f;
					}
					turnInd ();
					break;
			}

			//display collision warning
			if (recData.EmergencyBreaking) 
			{
				carRend.material = vehInFrontMaterial;		
			} else 
			{
				carRend.material = normalMaterial;
			}


			//display car speed
			string gear	= "";
			switch (recData.Gear) 
			{
			case 0:
				gear = "P";
				break;
			case 1:
				gear = "R";
				break;
			case 2:
				gear = "N";
				break;
			case 3:
				gear = "D";
				break;
			}
			gearText.text = gear;

        }
    }
}
