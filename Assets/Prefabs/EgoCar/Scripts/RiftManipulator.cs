﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiftManipulator : MonoBehaviour
{

    private Transform transf;
    private bool isDriverSeat = true;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        transf = GetComponent<Transform>();
    }

	/// <summary>
	/// Rotates the camera by the inverse angle of the actual EgoCar heading. 
	/// </summary>
	/// <param name="angle"></param>
    public void antiRotateY(float angle)
    {
        //Debug.Log("angle: " + angle);
        transf.Rotate(new Vector3(0, angle, 0), Space.Self);
        //transform.rotation = Quaternion.Euler(0, angle, 0);
    }

    private void FixedUpdate()
    {
        if (IsDriverSeat)
        {
            CompensateBellySteering();
        }
    }

    /// <summary>
    /// Moves the steering wheel away from the belly when the EgoCar is 
    /// turning around.
    /// </summary>
    private void CompensateBellySteering()
    {
        // Calculate the compensation
        float rotY = transform.localRotation.y * 180.0f / Mathf.PI;
        float maxComp = -0.16f;
        float rotPerc = Mathf.Abs(rotY / 50.0f);
        rotPerc = rotPerc > 1.0f ? 1.0f : rotPerc;
        float compPosition = rotPerc * maxComp;

        transform.localPosition = new Vector3(0, 0, compPosition);
    }

    /// <summary>
    /// Moves the Oculus camera.
    /// </summary>
    /// <param name="x">New x position.</param>
    /// <param name="y">New y position.</param>
    /// <param name="z">New z position.</param>
    public void moveCamera(float x, float y, float z)
    {
        transform.localPosition = new Vector3(x, y, z);
    }

    /// <summary>
    /// Sets this to driver seat active.
    /// </summary>
    public bool IsDriverSeat
    {
        get { return isDriverSeat; }
        set { isDriverSeat = value; }
    }
}
