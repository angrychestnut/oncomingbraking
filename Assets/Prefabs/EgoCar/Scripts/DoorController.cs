﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This object controls the doors on the EgoCar. It will communicate with ReceiveData
/// to check if any doors are open on the physical EgoCar. 
/// </summary>
public class DoorController : MonoBehaviour
{
    /// <summary>
    /// Knows if any door is open.
    /// </summary>
    public ReceiveData recData;
    /// <summary>
    /// To be set if the door is on the front.
    /// </summary>
    public bool front;
    /// <summary>
    /// To be set if the door is on the left side of the car.
    /// </summary>
    public bool left;
	/// <summary>
	/// Used for remembering the previous door angle.
	/// </summary>
    private float prevDoorAngle;
	/// <summary>
	/// Constant door angle value for open door.
	/// </summary>
	private const float DOOR_ANGLE = 60.0f;

    private void FixedUpdate()
    {
        float doorAngle = 0.0f;

        if (front && left) // FL Driver seat
        {
            doorAngle = recData.DoorFrontLeftOpen ? DOOR_ANGLE : 0.0f;
        }
        else if (front && !left) // FR Front passenger seat
        {
            doorAngle = recData.DoorFrontRightOpen ? -DOOR_ANGLE : 0.0f;
        }
        else if (!front && left) // RL Rear left passenger seat
        {
            doorAngle = recData.DoorRearLeftOpen ? DOOR_ANGLE : 0.0f;
        }
        else if (!front && !left) // RR Rear right passenger seat
        {
            doorAngle = recData.DoorRearRightOpen ? -DOOR_ANGLE : 0.0f;
        }

		// Check for avoiding unnecessary transformations/animations.
        if (doorAngle != prevDoorAngle)
        {
            transform.localRotation = Quaternion.Euler(0, 0, doorAngle);
        }
        prevDoorAngle = doorAngle;
    }
}
