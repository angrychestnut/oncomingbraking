﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Assets.Prefabs.EgoCar.Scripts;

namespace transferData
{
    public class SendData
    {

        CustomClientPipe objectDataPipe;

        /// <summary>
        /// Simple data container to make life simpler before sending data.
        /// See CameraModel for more documentation on variables.
        /// </summary>
        /// 

        public SendData(CustomClientPipe pipe)
        {
            objectDataPipe = pipe;
           
            //InitiateCANoeClient();
        }

        private struct Property
        {
            public string val;
            public string type;
        }

        private struct ObjectData
        {
            public Property track_ID;
            public Property track_status;
            public Property object_type;
            public Property width;
            public Property height;
            public Property vcs_long_pos;
            public Property vcs_lat_pos;
            public Property speed;
            public Property vcs_long_velocity;
            public Property vcs_long_accel;
            public Property f_oncoming;
            public Property f_oncomeable;
            public Property f_receding;
            public Property f_recedable;
            public Property object_cmbb_primary_conf;
            public Property object_cmbb_secondary_conf;
            public Property object_fcw_conf;
            public Property object_tja_conf;
            public Property hazard_light_indicator;
            public Property object_position_conf;
        }

        /// <summary>
        /// Sends data. Better name would be FormatData
        /// </summary>
        /// 
        public void sendData(List<DetectedObject> detectedObjectsList)
        {
            if (detectedObjectsList.Count == 0)
                return;

            Dictionary<string, ObjectData> formattedObjectData = new Dictionary<string, ObjectData>();

            foreach (DetectedObject detObj in detectedObjectsList)
            {
                ObjectData data = new ObjectData();
                int arrayIndex = detObj.ArrayIndex;

                data.track_status = new Property { val = Convert.ToString(detObj.TrackStatus), type = detObj.TrackStatus.GetType().Name };
                data.track_ID = new Property { val = Convert.ToString(detObj.TrackID), type = detObj.TrackID.GetType().Name };
                data.vcs_long_pos = new Property { val = Convert.ToString(detObj.VcsLongPos), type = detObj.VcsLongPos.GetType().Name };
                data.vcs_lat_pos = new Property { val = Convert.ToString(detObj.VcsLatPos), type = detObj.VcsLatPos.GetType().Name };
                data.speed = new Property { val = Convert.ToString(((DetectedObject)detObj).Speed), type = (((DetectedObject)detObj).Speed).GetType().Name };
                data.vcs_long_velocity = new Property { val = Convert.ToString(((DetectedObject)detObj).VcsLongVelocity), type = (((DetectedObject)detObj).VcsLongVelocity).GetType().Name };
                data.vcs_long_accel = new Property { val = Convert.ToString(((DetectedObject)detObj).VcsLongAccel), type = (((DetectedObject)detObj).VcsLongAccel).GetType().Name };
                data.object_type = new Property { val = Convert.ToString(detObj.ObjectType), type = detObj.ObjectType.GetType().Name };
                data.width = new Property { val = Convert.ToString(detObj.Width), type = detObj.Width.GetType().Name };
                data.height = new Property { val = Convert.ToString(detObj.Height), type = detObj.Height.GetType().Name };
                data.f_oncoming = new Property { val = Convert.ToString(((DetectedObject)detObj).FOncoming), type = (((DetectedObject)detObj).FOncoming).GetType().Name };
                data.f_oncomeable = new Property { val = Convert.ToString(((DetectedObject)detObj).FOncomeable), type = (((DetectedObject)detObj).FOncomeable).GetType().Name };
                data.f_receding = new Property { val = Convert.ToString(((DetectedObject)detObj).FReceding), type = (((DetectedObject)detObj).FReceding).GetType().Name };
                data.f_recedable = new Property { val = Convert.ToString(((DetectedObject)detObj).FRecedable), type = (((DetectedObject)detObj).FRecedable).GetType().Name };
                data.object_cmbb_primary_conf = new Property { val = Convert.ToString(detObj.ObjectCmbbPrimaryConf), type = detObj.ObjectCmbbPrimaryConf.GetType().Name };
                data.object_cmbb_secondary_conf = new Property { val = Convert.ToString(detObj.ObjectCmbbSecondaryConf), type = detObj.ObjectCmbbSecondaryConf.GetType().Name };
                data.object_fcw_conf = new Property { val = Convert.ToString(detObj.ObjectFcwConf), type = detObj.ObjectFcwConf.GetType().Name };
                data.object_tja_conf = new Property { val = Convert.ToString(detObj.ObjectTjaConf), type = detObj.ObjectTjaConf.GetType().Name };
                data.object_position_conf = new Property { val = Convert.ToString(detObj.ObjectPositionConf), type = detObj.ObjectPositionConf.GetType().Name };
                data.hazard_light_indicator = new Property { val = Convert.ToString(((DetectedObject)detObj).HazardLightIndicator), type = (((DetectedObject)detObj).HazardLightIndicator).GetType().Name };

                formattedObjectData.Add("obj_" + arrayIndex.ToString(), data);
            }

            objectDataPipe.WriteData(JsonConvert.SerializeObject(formattedObjectData));
        }
    }
}
