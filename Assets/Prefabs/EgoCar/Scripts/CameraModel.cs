﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using transferData;
using UnityEngine.UI;
using Assets.Prefabs.EgoCar.Scripts;
using System;

/// <summary>
/// CameraModel is a simulated camera for the test car.
/// With the simulated camera it is possible to detect objects in 
/// front of the test car. 
/// It's required that other objects that need to be detects must
/// have a colliding box and a tag.
/// </summary>
public class CameraModel : MonoBehaviour
{
	public SendData dataSender;
	/// <summary>
	/// For sending feedbar to VR. Really simple.
	/// </summary>
	public ScreenConfigDriverDisplay screenConfigDriverDisplay;

	/// <summary>
	/// For storing detected objects in front of the ego car.
	/// </summary>
	private Dictionary<Collider, DetectedObject> detectedObjects;

	public ReceiveData recData;

	public enum ObjectType
	{
		// (*) Should be able to model them in the same way.
		ObjectUndetermined,
		Car, // (*)
		MotorCycle, // (*)
		Truck, // (*)
		Pedestrian,
		Pole,
		Tree,
		Animal,
		GenerlOnRoad_ObjectDetected,
		Bicycle,
		VehicleUnidentified,
	}

	/// <summary>
	/// For storing injection data signals to be displayed.
	/// </summary>
	public Text injText;
	public bool injTextFlag = false;

	void FixedUpdate()
	{
		List<DetectedObject> list = new List<DetectedObject>(detectedObjects.Values);

		dataSender.sendData(list);

		if (Input.GetKey("i"))
		{
			if (!injTextFlag)
			{
				injText.enabled = true;
				injTextFlag = true;
			}
			else
			{
				injText.enabled = false;
				injTextFlag = false;
			}			
		}
	}

	// Use this for initialization
	void Start()
	{
		CustomClientPipe virtualObjectPipe = new CustomClientPipe("VirtualObjectPipe");
		CustomClientPipe vehicleSignalPipe = new CustomClientPipe("VehicleSignalPipe");

		virtualObjectPipe.Connect();
		dataSender = new SendData(virtualObjectPipe);

		vehicleSignalPipe.Connect();
		recData.addPipe(vehicleSignalPipe);
		// Start a task to receive vehicle signal data asynchronously 
		recData.receiveSignalData();

		detectedObjects = new Dictionary<Collider, DetectedObject>();
	}

	/// <summary>
	/// OnTriggerEnter is called when the Collider other enters the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerEnter(Collider other)
	{
		if (detectedObjects.ContainsKey(other))
		{
			return;
		}

		DetectedObject detObj = null;

		if (other.tag == "Car")
		{
			detObj = new DetectedObject();
			//object_type
			detObj.ObjectType = (uint)ObjectType.Car;
		}
		else if (other.tag == "MotorCycle")
		{
			detObj = new DetectedObject();
			//object_type
			detObj.ObjectType = (uint)ObjectType.MotorCycle;
		} //add support for more objectypes here!
		else if (other.tag == "Pedestrian")
		{
			detObj = new DetectedObject();
			detObj.ObjectType = (uint)ObjectType.Pedestrian;
		}
		else
		{
			return;
		}

		// Get the size of detected object
		Vector3 otherSize = other.bounds.size;
		float width = Mathf.Abs(otherSize.x); 
		float height = Mathf.Abs(otherSize.y); // 0.8 because of the bounding box. SHOULD BE FIXED. Expected height of XC60 is 1.65

        //width, height
        detObj.Height = height;
		detObj.Width = width;

		detectedObjects.Add(other, detObj);

		// Send feeback to DIM
		screenConfigDriverDisplay.setDriverDisplayCarInFront(true);
	}

	/// <summary>
	/// OnTriggerStay is called once per frame for every Collider other
	/// that is touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerStay(Collider other)
	{
		if (detectedObjects.ContainsKey(other))
		{
			// Compute the distance to the vehicle in front.
			//Vector3 thisPos = egoCollider.transform.position;
			Vector3 thisPos = transform.parent.position;
			Vector3 otherPos = other.transform.position;

			//get the local coordinates for the egocar and the detected object
			Vector3 localCoordThis = transform.InverseTransformDirection(thisPos);
			Vector3 localCoordOther = transform.InverseTransformDirection(otherPos);

			if (other.tag == "Car" || other.tag == "Pedestrian")
			{
				float OFFSET_OBJECT = 0;
				//offset to the middle of the object. Longitudinally
				if (other.tag == "Car") 
				{
					OFFSET_OBJECT = 4.69f; // length of xc60 ego car
				} else if (other.tag == "Pedestrian") 
				{
					OFFSET_OBJECT = 2.6f;
				}

				DetectedObject detObj = detectedObjects[other];        

				//vcs_long_pos
				detObj.VcsLongPos = Math.Abs(localCoordOther.z - localCoordThis.z) - OFFSET_OBJECT;

				//vcs_lat_pos
				detObj.VcsLatPos = localCoordThis.x - localCoordOther.x;

				//Speed of the detected car
				float rbVelVecComp = 1e-2f;
				Rigidbody rigidBodyDetObj = other.GetComponent<Rigidbody>();
				Vector3 velVec = transform.InverseTransformDirection(rigidBodyDetObj.velocity);
				float spdDetObj = velVec.magnitude;

				//speed
				float spdEgoCar = ((float)recData.EgoCarSpeed) / 3.6f;
				detObj.Speed = spdDetObj > rbVelVecComp ? spdDetObj : 0;
				
				//vcs_long_velocity
				float longVel = Math.Abs(velVec.z) > rbVelVecComp ? (-velVec.z) : 0; //car is in negative z axis
				
				//vcs_lat_velocity
				float latVel = Math.Abs(velVec.y) > rbVelVecComp ? (-velVec.y) : 0;
				
				 //vcs_long_accel 
                float long_acc = (longVel - detObj.VcsLongVelocity) / Time.fixedDeltaTime; //current vel - last vel		
				detObj.VcsLongAccel = long_acc;

                //vcs_lat_accel 
                float lat_acc = (latVel - detObj.VcsLatVelocity) / Time.fixedDeltaTime;
                detObj.VcsLatAccel = lat_acc;

				detObj.VcsLongVelocity = longVel;
				detObj.VcsLatVelocity = latVel;

				//f_oncoming
				detObj.FOncoming = false; //hardcoded value for now

				//f_recedable
				detObj.FReceding = false; //hardcoded value for now

                // 0 - unknown
                uint hazardLight = 0;
                if (other.tag == "Car")
                {
                    //Check if the hazard light is on
                    GameObject targetCar = other.gameObject;
                    hazardLight = targetCar.GetComponent<carConfig>().hazardLight == false ? (uint)1 : (uint)2;
                    //hazard_light_indicator
                }
                detObj.HazardLightIndicator = hazardLight;

                // Set UI Canvas text
                setInjText(detObj);
			}
		}
	}

	/// <summary>
	/// OnTriggerExit is called when the Collider other has stopped touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerExit(Collider other)
	{

		if (detectedObjects.ContainsKey(other))
		{
			detectedObjects.Remove(other);

			// Send feedback to DIM
			screenConfigDriverDisplay.setDriverDisplayCarInFront(detectedObjects.Keys.Count > 0);
		}
	}

	/// <summary>
	/// Used for setting the text value in the UI Canvas.
	/// </summary>
	public void setInjText(DetectedObject obj)
	{
		if (injText != null)
		{
			injText.text = "INJECTION DATA\n";
			injText.text += "===================\n";

			int i = 0;
			foreach (var entry in obj.GetType().GetProperties())
			{
				injText.text += i.ToString() + ":	" +entry.Name + " = " + entry.GetValue(obj, null).ToString() + "\n";
				i++;
			}
		}
	}
}

public class DetectedObject
{
    /* Will never be -1, since we increase the arrayIndex in the constructor.*/
    private static int arrayIndex = -1;
    private int myArrayIndex;

    public DetectedObject()
    {
        arrayIndex += 1;
        arrayIndex = arrayIndex % 32;
        myArrayIndex = arrayIndex;
    }

    /// <summary>
    /// Indicates the status of the detected object 0 = Invalid, 1 = Merged, 2 = New, 3 = New Coasted, - 1 = New Updated, - 1 = Updated, - 1 = Coasted, 7 = Reserved
    /// </summary>
    private uint track_status = 5;

    public uint TrackStatus
    {
        get { return track_status; }
        set { track_status = value; }
    }

    /// <summary>
    /// The unique track ID of  the detected object. [0 31] NOTE: 
    /// </summary>
    private static uint track_ID = 1;

    public uint TrackID
    {
        get { return track_ID; }
        set { track_ID = value; }
    }

    public int ArrayIndex
    {
        get { return myArrayIndex; }
    }

    /// <summary>
    /// Determines the type of Object detected 0 = Object Undetermined, 1 = Car, 2 = MotorCycle, 3 = Truck. object_type
    /// </summary>
    private uint object_type;

    public uint ObjectType
    {
        get { return object_type; }
        set { object_type = value; }
    }

    /// <summary>
    /// Object's width and height. [0 10] m and [0 5] m respectively. width. height
    /// </summary>
    private float width, height;

    public float Width
    {
        get { return width; }
        set { width = value; }
    }

    public float Height
    {
        get { return height; }
        set { height = value; }
    }

    /// <summary>
    ///Longitudinal distance to object from host, [0 255] m. vcs_long_pos
    /// </summary>
    private float vcs_long_pos;

    public float VcsLongPos
    {
        get { return vcs_long_pos; }
        set { vcs_long_pos = value; }
    }
    /// <summary>
    ///The lateral distance of the detected object from the host vehicle., [-50 50] m. vcs_lat_pos
    /// </summary>
    private float vcs_lat_pos;

    public float VcsLatPos
    {
        get { return vcs_lat_pos; }
        set { vcs_lat_pos = value; }
    }

    /// <summary>
    ///The Confidence is reported as 0xF0 = No Confidence, 0x69 = Low Confidence, 0x96 =Medium Confidence, 0xC3=High Confidence (ASIL B Signal.). object_cmbb_primary_conf
    /// </summary>
    private uint object_cmbb_primary_conf = 195;

    public uint ObjectCmbbPrimaryConf
    {
        get { return object_cmbb_primary_conf; }
        set { object_cmbb_primary_conf = value; }
    }

    /// <summary>
    ///The Confidence is reported as 0 = Not Reliable, 1 = Reliable (ASIL A Signal.) object_cmbb_secondary_conf
    /// </summary>
    private uint object_cmbb_secondary_conf = 1;

    public uint ObjectCmbbSecondaryConf
    {
        get { return object_cmbb_secondary_conf; }
        set { object_cmbb_secondary_conf = value; }
    }

    /// <summary>
    ///The Confidence is reported as 0 = No Confidence, 1 = High Confidence. object_fcw_conf
    /// </summary>
    private uint object_fcw_conf = 1;

    public uint ObjectFcwConf
    {
        get { return object_fcw_conf; }
        set { object_fcw_conf = value; }
    }

    /// <summary>
    ///The Confidence is reported as 0 = No Confidence, 1 = Reliable 1, 2 = Reliable 2. object_tja_conf
    /// </summary>
    private uint object_tja_conf = 2;

    public uint ObjectTjaConf
    {
        get { return object_tja_conf; }
        set { object_tja_conf = value; }
    }

    /// <summary>
    ///The Confidence is reported as 0 = No Confidence, 1 = Reliable 1, 2 = Reliable 2. object_position_conf
    /// </summary>
    private uint object_position_conf = 2;

    public uint ObjectPositionConf
    {
        get { return object_position_conf; }
        set { object_position_conf = value; }
    }
    
	/// <summary>
	/// Longitudinal speed of object. [-100 100] m/s.
	/// </summary>
	private float speed = 0;

	public float Speed
	{
		get { return speed; }
		set { speed = value; }
	}

	/// <summary>
	/// Longitudinal speed relative to ground in host's direction. [0 100] m/s.
	/// </summary>
	private float vcs_long_velocity;

	public float VcsLongVelocity
	{
		get { return vcs_long_velocity; }
		set { vcs_long_velocity = value; }
	}

	/// <summary>
	/// Lateral speed relative to ground in host's direction. [0 100] m/s.
	/// </summary>
	private float vcs_lat_velocity;

	public float VcsLatVelocity
	{
		get { return vcs_lat_velocity; }
		set { vcs_lat_velocity = value; }
	}

	/// <summary>
	/// Longitudinal acceleration of object. [-20 20] m/s^2 .
	/// </summary>
	private float vcs_long_accel;

	public float VcsLongAccel
	{
		get { return vcs_long_accel; }
		set { vcs_long_accel = value; }
	}

	/// <summary>
	/// Lateral acceleration of object. [-20 20] m/s^2 .
	/// </summary>
	private float vcs_lat_accel;

	public float VcsLatAccel
	{
		get { return vcs_lat_accel; }
		set { vcs_lat_accel = value; }
	}

	/// <summary>
	/// Indicates the state of the obstacles hazard light. false = No hazard light is active. true = Hazard light is steady active. hazard_light_indicator
	/// </summary>
	private uint hazard_light_indicator;

	public uint HazardLightIndicator
	{
		get { return hazard_light_indicator; }
		set { hazard_light_indicator = value; }
	}

	/// <summary>
	/// Set to true when the object is oncoming to the subject vehicle. f_oncoming
	/// </summary>
	private bool f_oncoming;

	public bool FOncoming
	{
		get { return f_oncoming; }
		set
		{
			f_oncoming = value;

			//oncomeable should keep its true value if the object has been oncoming sometime in the past
			if (value)
			{
				f_oncomeable = true;
			}
		}
	}

	/// <summary>
	/// Set to true if the object is oncoming or has been observed to be oncoming  in the past. f_oncomeable
	/// </summary>
	private bool f_oncomeable = false;

	public bool FOncomeable
	{
		get { return f_oncomeable; }
		set { f_oncomeable = value; }
	}

	/// <summary>
	/// Set to true when the object is moving away from the subject vehicle. f_receding
	/// </summary>
	private bool f_receding;

	public bool FReceding
	{
		get { return f_receding; }
		set
		{
			f_receding = value;

			//recedable should keep its true value if the object has been receding sometime in the past
			if (value)
			{
				this.f_recedable = true;
			}
		}
	}

	/// <summary>
	/// Set to true if the object is receding or has been observed to recede in the past. f_recedable
	/// </summary>
	private bool f_recedable = false;

	public bool FRecedable
	{
		get { return f_recedable; }
		set { f_recedable = value; }
	}
}
