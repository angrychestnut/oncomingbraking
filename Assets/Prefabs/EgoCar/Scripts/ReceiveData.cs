﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Assets.Prefabs.EgoCar.Scripts;
using System.Threading.Tasks;

/// <summary>
/// Receives data from the physical EgoCar and updates content in the VR
/// EgoCar such as vehicle speed and steering wheel angle.
/// </summary>
public class ReceiveData : MonoBehaviour
{
    /// <summary>
    /// The steering wheel angle.
    /// 0 car is driving forward.
    /// < 0: car is turning right.
    /// > 0: car is turning left.
    /// </summary>
    private static float steeringWheelAngle = 0.0f;

    /// <summary>
    /// The ego car's speed in kph.
    /// </summary>
    private static UInt16 egoCarSpeed = 0;
    /// <summary>
    /// Doors.
    /// </summary>
    private static bool doorFrontLeftOpen = false;
    private static bool doorFrontRightOpen = false;
    private static bool doorRearLeftOpen = false;
    private static bool doorRearRightOpen = false;
    /// <summary>
    /// Used for setting the left side temperature in infotainment.
    /// </summary>
    private static int temperatureLeft = 0;
    /// <summary>
    /// Used for setting the right side temperature in infotainment.
    /// </summary>
    private static int temperatureRight = 0;
    /// The value of the turn indicator signal
    /// </summary>
	private static byte turnInd = 0;
	/// <summary>
	/// Emergency breaking indication
	/// </summary>
	private static bool emergencyBreaking = false;
	/// The value of the turn indicator signal
	/// </summary>
	private static byte gear = 0;
	/// <summary>
	/// Vehicle Signal Pipe Client: Receives vehicle signal data from Real-time platform
	/// </summary>
    private CustomClientPipe signalDataPipe;    

    // Use this for initialization
    void Start()
    {
        this.EgoCarSpeed = 0;
        this.SteeringWheelAngle = 0;
    }

	public void addPipe(CustomClientPipe pipe)
	{
		signalDataPipe = pipe;
	}

    public void receiveSignalData()
    {
        Task.Run(async () =>
        {

            while (true)
            {
                try
                {
                    var json_signals = await signalDataPipe.ReadDataAsync();
                    // NOTE: Call the unity read data function here with json_signals (Json dict) as function argument
                    JsonUtility.ToJson(json_signals);
                    FDXVehicleSignals signals = JsonUtility.FromJson<FDXVehicleSignals>(json_signals);

                    SteeringWheelAngle = signals.SteerWhlAgSafe;
                    EgoCarSpeed = signals.VehSpdIndcdVal;

                    // Signals for doors
                    DoorFrontLeftOpen = signals.DoorDrvrSts == 1;
                    DoorFrontRightOpen = signals.DoorPassSts == 1;
                    DoorRearLeftOpen = signals.DoorDrvrReSts == 1;
                    DoorRearRightOpen = signals.DoorPassReSts == 1;

                    // Signals for temperature (infotainment)
                    TemperatureLeft = signals.HmiCmptmtTSpForRowFirstLe;
                    //TemperatureLeft = signals.HmiCmptmtTSpForRowSecLe;
                    TemperatureRight = signals.HmiCmptmtTSpForRowFirstRi;
                    //TemperatureRight = signals.HmiCmptmtTSpForRowSecRi;
                    
					TurnIndicator = signals.IndcrTurnSts1WdSts;
					
					EmergencyBreaking = signals.CllsnFwdWarnReq == 0 ? false : true;

					Gear = signals.GearLvrIndcn;					
                }
                catch (Exception e)
                {
                    //Debug.LogError(e);
                }
            }
        });
    }

    /// <summary>
    /// If set, the steering wheel angle will be updated in SteeringWheelTurning.
    /// </summary>
    /// <returns>Current steering wheel angle.</returns>
    public float SteeringWheelAngle
    {
        get { return steeringWheelAngle; }
        set
        {
            float stAngle = value / 1000;
            steeringWheelAngle = stAngle * 180.0f / (float)Math.PI;
        }
    }

    /// <summary>
    /// Getter and setter for EgoCar speed.
    /// </summary>
    /// <returns></returns>
    public UInt16 EgoCarSpeed
    {
		get { return egoCarSpeed; }
        set { egoCarSpeed = value; }
    }

    /// <summary>
    /// Getter and setter for front left door.
    /// </summary>
    /// <returns>True if the door is open.</returns>
    public bool DoorFrontLeftOpen
    {
        get { return doorFrontLeftOpen; }
        set { doorFrontLeftOpen = value; }
    }

    /// <summary>
    /// Getter and setter for front right door.
    /// </summary>
    /// <returns>True if the door is open.</returns>
    public bool DoorFrontRightOpen
    {
        get { return doorFrontRightOpen; }
        set { doorFrontRightOpen = value; }
    }

    /// <summary>
    /// Getter and setter for rear left door.
    /// </summary>
    /// <returns>>True if the door is open.</returns>
    public bool DoorRearLeftOpen
    {
        get { return doorRearLeftOpen; }
        set { doorRearLeftOpen = value; }
    }

    /// <summary>
    /// Getter and setter for rear right door.
    /// </summary>
    /// <returns>True if the door is open.</returns>
    public bool DoorRearRightOpen
    {
        get { return doorRearRightOpen; }
        set { doorRearRightOpen = value; }
    }

    /// <summary>
    /// Getter and setter for the left desired temperature in the infotainment.
    /// </summary>
    /// <returns>Current desired temperature.</returns>
    public int TemperatureLeft
    {
        get { return temperatureLeft; }
        set { temperatureLeft = value; }
    }

    /// <summary>
    /// Getter and setter for the right desired temperature in the infotainmetn.
    /// </summary>
    /// <returns>Current desired temperature.</returns>
    public int TemperatureRight
    {
        get { return temperatureRight; }
        set { temperatureRight =  value;}
    }

	/// <summary>
	/// Getter and setter for turnIndicator value. 0 - no turn ind, 1 - turn left ind, 2 - turn right ind
	/// </summary>
	/// <returns></returns>
	public byte TurnIndicator
	{
		get { return turnInd; }
		set
		{
			turnInd = value;
		}
	}

	public bool EmergencyBreaking 
	{
		get { return emergencyBreaking; }
		set 
		{
			emergencyBreaking = value;
		}
	}

	public byte Gear
	{
		get { return gear; }
		set 
		{
			gear = value;
		}
	}
    /// <summary>
    /// Signal data object that contains the signals received.
    /// </summary>
    public class FDXVehicleSignals
    {
        // FDX Group ID 1
        /// <summary>
        /// Vehicle speed indicated value in DIM, kph.
        /// </summary>
        public UInt16 VehSpdIndcdVal;
        /// <summary>
        /// Engine speed in RMP
        /// </summary>
        public UInt16 EngN;
        /// <summary>
        /// Usage mode.
        /// 0   Abandoned
        /// 1   Inactive
        /// 2   Convinience
        /// 11  Active
        /// 13  Driving
        /// </summary>
        public byte UsgModSts;
        /// <summary>
        /// Steering wheel angle. in radians.
        /// </summary>
        public Int16 SteerWhlAgSafe;
        /// <summary>
        /// Indicator, 0 - Off, 1 - Left, 2 - Right.
        /// </summary>
        public byte IndcrTurnSts1WdSts;
        /// <summary>
        /// Parking break. 
        /// </summary>
        public byte TrsmParkLockd;
        /// <summary>
        /// Gear position.
        /// </summary>
        public byte GearLvrIndcn;
        /// <summary>
        /// Front left (driver seat) door,  0 - Unknown, 1 - Open, 2 - Closed.
        /// </summary>
        public byte DoorDrvrSts;
        /// <summary>
        /// Front right door,  0 - Unknown, 1 - Open, 2 - Closed.
        /// </summary>
        public byte DoorPassSts;
        /// <summary>
        /// Rear right door door,  0 - Unknown, 1 - Open, 2 - Closed.
        /// </summary>
        public byte DoorDrvrReSts;
        /// <summary>
        /// Rear left door,  0 - Unknown, 1 - Open, 2 - Closed.
        /// </summary>
        public byte DoorPassReSts;
        /// <summary>
        /// Belt indicator.
        /// </summary>
        public byte BltLockStAtPassForBltLockSt1;
        /// <summary>
        /// Belt indicator.
        /// </summary>
        public byte BltLockStAtRowSecLeForBltLockSt1;
        /// <summary>
        /// Belt indicator.
        /// </summary>
        public byte BltLockStAtRowSecRiForBltLockSt1;
        /// <summary>
        /// Emergency breaking indication. 0/1
        /// </summary>
        public byte CllsnFwdWarnReq;
        /// <summary>
        /// Breaking indication.
        /// </summary>
        public byte AsySftyDecelReq;
        /// <summary>
        /// Collision threat.
        /// </summary>
        public byte CllsnThreat;
        /// <summary>
        /// First row, left side temperature in degrees celsius
        /// </summary>
        public byte HmiCmptmtTSpForRowFirstLe;
        /// <summary>
        /// First row, right side temperature in degrees celsius
        /// </summary>
        public byte HmiCmptmtTSpForRowFirstRi;
        /// <summary>
        /// Second row, left side temperature in degrees celsius
        /// </summary>
        public byte HmiCmptmtTSpForRowSecLe;
        /// <summary>
        /// Secondrow, left side temperature in degrees celsius
        /// </summary>
        public byte HmiCmptmtTSpForRowSecRi;
    }
}
