﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnComingTrigger : MonoBehaviour {

    private Rigidbody rb;
    private bool trigger = false;
    public bool PipeConnected = false;
    public float acceleration;
    private Vector3 currentPos, prevPos;
    private Vector3 currentVel, prevVel;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        if(!PipeConnected)
        {
            currentPos = GetComponent<Transform>().position;
            prevPos = new Vector3();
            currentVel = new Vector3(0, 0, 0);
            prevVel = new Vector3();
        }
        
    }
    void Update()
    {
        if (!PipeConnected)
        {
            if (!trigger)
            {
                Move(ref prevPos, ref currentPos, ref prevVel, ref currentVel, acceleration);
                if (Mathf.Abs(currentVel.z) >= 50 / 3)
                {
                    trigger = true;
                    Debug.Log("On Coming Trigger Activated!");
                }
            }
            else
            {
                Move(ref prevPos, ref currentPos, ref prevVel, ref currentVel, 0);
            }
        }
        else
        {
            uint speed = GetComponent<ReceiveData>().EgoCarSpeed;
            Debug.Log("Velocity is " + speed +" kph");
            if (Mathf.Abs(speed)>=60)
            {
                trigger = true;
                Debug.Log("On Coming Trigger Activated!");
            }
        }

    }

    private void Move(ref Vector3 prevPos, ref Vector3 currentPos, ref Vector3 prevVel, ref Vector3 currentVel, float acceleration)
    {
        prevPos = currentPos;
        double d = currentVel.z * Time.deltaTime - 0.5 * acceleration * Mathf.Pow(Time.deltaTime, 2);
        currentPos = prevPos + new Vector3(0, 0, (float)d);
        GetComponent<Transform>().position = currentPos;
        prevVel = currentVel;
        currentVel = prevVel + new Vector3(0, 0, -acceleration * Time.deltaTime);
    }

    public bool Trigger
    {
        get { return trigger; }
        set { trigger = value; }
    }

}
