using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardController : MonoBehaviour {
	public float speed = 5;
	public float rotSpeed = 25;
	private Rigidbody rb;
    public KeyCode Up;
    public KeyCode Down;
    public KeyCode Left;
    public KeyCode Right;
    void Start ()
    {		
		rb = GetComponent<Rigidbody>();
    }

	// Update is called fixed framerate frame
	void FixedUpdate () {
		if (Input.GetKey(Up))
            rb.MovePosition(transform.position + rb.transform.forward * speed * Time.deltaTime);

        if (Input.GetKey(Down))
           rb.MovePosition(transform.position - rb.transform.forward * speed * Time.deltaTime);
    }	
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(Left))
            transform.Rotate(new Vector3(0, -rotSpeed, 0) * Time.deltaTime, Space.Self );

        if (Input.GetKey(Right))
            transform.Rotate(new Vector3(0, rotSpeed, 0) * Time.deltaTime, Space.Self);
	}	
}
