﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMotion : MonoBehaviour {

    public GameObject targetCar;
    public GameObject EgoCar;
    private Rigidbody rb;
    public Vector3 targetSpeed;
    private Vector3 lastPos;
    private Vector3 currentPos;
    private bool startToMove = false;

	void Start ()
    {
        rb = targetCar.GetComponent<Rigidbody>();
        currentPos = targetCar.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!startToMove)
        {
            lastPos = currentPos;
            currentPos = targetCar.transform.position;
            if ((currentPos - lastPos).magnitude < 0.00001)
            {
                startToMove = true;
            }
        }
        else
        {
            if(EgoCar.GetComponent<OnComingTrigger>().Trigger)
            {
                lastPos = currentPos;
                rb.velocity.Set(targetSpeed.x, targetSpeed.y, targetSpeed.z);
                currentPos = lastPos + targetSpeed * Time.deltaTime;
                targetCar.transform.position = currentPos;
            }           
        }

    }
}
