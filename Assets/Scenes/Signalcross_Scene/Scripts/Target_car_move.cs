﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target_car_move : MonoBehaviour {

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    public void Move()
    {
        rb.velocity = transform.forward * 15;
    }

}
