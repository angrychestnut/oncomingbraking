﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossing_Trigger : MonoBehaviour
{
    public Target_car_move TargetCarScript;

    //private Rigidbody rb;
    private GameObject Targetcar;

    void Start()
    {
        Targetcar = GameObject.Find("TargetCar");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != GameObject.Find("RayCam"))
        {
            TargetCarScript = Targetcar.GetComponent<Target_car_move>();
            TargetCarScript.Move();
        }
        
        
    }
}

