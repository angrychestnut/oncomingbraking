﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootMotionScript : MonoBehaviour {

    public GameObject Sam;
    private Rigidbody rb;
    public float SamSpeed;

    void Start()
    {
        rb = Sam.GetComponent<Rigidbody>();
    }

    void OnAnimatorMove()
    {
        Animator animator = Sam.GetComponent<Animator>();

        if (animator && (animator.GetBool("Car_passing")) && (!(animator.GetBool("Stop_Running"))))
        {
            rb.velocity = Sam.transform.forward * SamSpeed;
        }

        if ((animator.GetBool("Stop_Running")) && (animator.GetBool("Car_passing")))
        {
            animator.SetBool("Car_passing" , false);
            rb.velocity = Vector3.zero;
        }
    }


}

