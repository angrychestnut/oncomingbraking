﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EgocarTrigger : MonoBehaviour {

    public GameObject Sam;
    private Animator animator;
    private Collider CrossingDetector;

    void Start()
    {
        CrossingDetector = GetComponent<Collider>();
        animator = Sam.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != GameObject.Find("RayCam"))
            animator.SetBool("Car_passing", true);
    }

}
