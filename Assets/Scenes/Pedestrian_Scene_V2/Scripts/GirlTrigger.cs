﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlTrigger : MonoBehaviour
{

    public GameObject SportyGirl;
    private Animator animator;
    private Collider CrossingDetector;

    void Start()
    {
        CrossingDetector = GetComponent<Collider>();
        animator = SportyGirl.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("STOP");
        if (other.gameObject != GameObject.Find("RayCam"))
            animator.SetBool("Car_passing", true);
        //animator.SetBool("Stop_Running", true);
    }
}
