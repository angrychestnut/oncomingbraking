﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sporty_Stop : MonoBehaviour {

    public GameObject SportyGirl;
    private Animator animator;
    private Collider CrossingDetector;

    void Start()
    {
        CrossingDetector = GetComponent<Collider>();
        animator = SportyGirl.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != GameObject.Find("RayCam"))
            animator.SetBool("Stop_Running", true);
    }
}
