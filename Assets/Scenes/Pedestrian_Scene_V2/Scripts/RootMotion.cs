﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootMotion : MonoBehaviour {

    public GameObject SportyGirl;
    private Rigidbody rb;

    void Start()
    {
        rb = SportyGirl.GetComponent<Rigidbody>();
    }

    // Animator for Sam
    void OnAnimatorMove()
    {
        Animator animator = SportyGirl.GetComponent<Animator>();

        if (animator && (animator.GetBool("Car_passing")) && (!(animator.GetBool("Stop_Running"))))
        {
            rb.velocity = SportyGirl.transform.forward * 4;
        }

        if ((animator.GetBool("Stop_Running")) && (animator.GetBool("Car_passing")))
        {
            animator.SetBool("Car_passing" , false);
            rb.velocity = Vector3.zero;
        }
    }

}

